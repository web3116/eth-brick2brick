// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

contract CrowdFunding {
    address private owner;
    struct Contribution {
        address contributor;
        uint value;
    }

    struct Project {
        string id;
        string name;
        string description;
        address payable author;
        bool state;
        uint256 funds;
        uint256 fundraisingGoal;
    }

    Project[] public projects;
    uint public totalProjects;
    mapping( string => Contribution[]) public contribution;

    event ProjectFunded(string projectId, uint256 value);
    event ProjectStateChanged(string id, bool state);
    event ProjectCreated(string id, string name, string description, uint fundraisingGoal);

    constructor() {
        owner = 0x10Efe72397788350eDf14640c270D25d604A96EB;
    }

    function createProject(string calldata id, string calldata name, string calldata description, uint fundraisingGoal) public payable  {
        require(
            fundraisingGoal > 0,
            "La meta debe ser mayor a cero"
        );
        Project memory project = Project(
            id,
            name,
            description,
            payable(owner),
            true,
            0,
            fundraisingGoal
        ); 
        projects.push(project);
        totalProjects = projects.length;
        emit ProjectCreated(id, name, description, fundraisingGoal);
    }

    function fundProject(uint proyectIndex) public payable {
        Project memory project = projects[proyectIndex];
        require(
            project.state,
            "El proyecto esta cerrado"
        );

        require(
            msg.value > 0,
            "El valor debe ser mayor a cero"
        );

        if(project.state) {
            project.author.transfer(msg.value);
            project.funds += msg.value;
            projects[proyectIndex] = project;

            contribution[project.id].push(Contribution(msg.sender, msg.value));

            emit ProjectFunded(project.id, msg.value);
        }
        
    }

    modifier onlyOwner() {
        require(msg.sender == owner, "No tiene permisos para cambiar el proyecto");
        //la función es insertada en donde aparece este símbolo
        _;
    }

    function changeProjectState(uint proyectIndex,bool newState) public onlyOwner {
        Project memory project = projects[proyectIndex];
        require(
            project.state != newState,
            "El estado debe ser diferente al actual"
        );
        project.state = newState;
        emit ProjectStateChanged(project.id, newState);
    }
}