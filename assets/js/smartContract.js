// window.ethereum.enable()
window.localStorage

if (typeof window.ethereum !== "undefined") {
	console.log("MetaMask is installed!")
}

const provider = new ethers.providers.Web3Provider(window.ethereum, "goerli")
const ContractAddress = "0xE8D7b563913c25FD7433F61e82cD5Efd2712d61E"
const ContractABI = [
	{
		inputs: [],
		stateMutability: "nonpayable",
		type: "constructor",
	},
	{
		anonymous: false,
		inputs: [
			{
				indexed: false,
				internalType: "string",
				name: "id",
				type: "string",
			},
			{
				indexed: false,
				internalType: "string",
				name: "name",
				type: "string",
			},
			{
				indexed: false,
				internalType: "string",
				name: "description",
				type: "string",
			},
			{
				indexed: false,
				internalType: "uint256",
				name: "fundraisingGoal",
				type: "uint256",
			},
		],
		name: "ProjectCreated",
		type: "event",
	},
	{
		anonymous: false,
		inputs: [
			{
				indexed: false,
				internalType: "string",
				name: "projectId",
				type: "string",
			},
			{
				indexed: false,
				internalType: "uint256",
				name: "value",
				type: "uint256",
			},
		],
		name: "ProjectFunded",
		type: "event",
	},
	{
		anonymous: false,
		inputs: [
			{
				indexed: false,
				internalType: "string",
				name: "id",
				type: "string",
			},
			{
				indexed: false,
				internalType: "bool",
				name: "state",
				type: "bool",
			},
		],
		name: "ProjectStateChanged",
		type: "event",
	},
	{
		inputs: [
			{
				internalType: "uint256",
				name: "proyectIndex",
				type: "uint256",
			},
			{
				internalType: "bool",
				name: "newState",
				type: "bool",
			},
		],
		name: "changeProjectState",
		outputs: [],
		stateMutability: "nonpayable",
		type: "function",
	},
	{
		inputs: [
			{
				internalType: "string",
				name: "",
				type: "string",
			},
			{
				internalType: "uint256",
				name: "",
				type: "uint256",
			},
		],
		name: "contribution",
		outputs: [
			{
				internalType: "address",
				name: "contributor",
				type: "address",
			},
			{
				internalType: "uint256",
				name: "value",
				type: "uint256",
			},
		],
		stateMutability: "view",
		type: "function",
	},
	{
		inputs: [
			{
				internalType: "string",
				name: "id",
				type: "string",
			},
			{
				internalType: "string",
				name: "name",
				type: "string",
			},
			{
				internalType: "string",
				name: "description",
				type: "string",
			},
			{
				internalType: "uint256",
				name: "fundraisingGoal",
				type: "uint256",
			},
		],
		name: "createProject",
		outputs: [],
		stateMutability: "payable",
		type: "function",
	},
	{
		inputs: [
			{
				internalType: "uint256",
				name: "proyectIndex",
				type: "uint256",
			},
		],
		name: "fundProject",
		outputs: [],
		stateMutability: "payable",
		type: "function",
	},
	{
		inputs: [
			{
				internalType: "uint256",
				name: "",
				type: "uint256",
			},
		],
		name: "projects",
		outputs: [
			{
				internalType: "string",
				name: "id",
				type: "string",
			},
			{
				internalType: "string",
				name: "name",
				type: "string",
			},
			{
				internalType: "string",
				name: "description",
				type: "string",
			},
			{
				internalType: "address payable",
				name: "author",
				type: "address",
			},
			{
				internalType: "bool",
				name: "state",
				type: "bool",
			},
			{
				internalType: "uint256",
				name: "funds",
				type: "uint256",
			},
			{
				internalType: "uint256",
				name: "fundraisingGoal",
				type: "uint256",
			},
		],
		stateMutability: "view",
		type: "function",
	},
]
let signer, Contract
let totalProjects = 5
// connect wallet
async function connectWallet() {
	console.log('connect wallet')
	const accounts = await ethereum.request({
		method: "eth_requestAccounts",
	})
	const account = accounts[0]
	console.log(accounts)
	signer = provider.getSigner(account)
	Contract = new ethers.Contract(ContractAddress, ContractABI, signer)
	$('#account').html(account)
	$('#contenHome').show()
	$('#signinOff').hide()
	getProjects()
	
}

// create project
async function createProject(id, name, description, goal){
	createProjectPromise = Contract.createProject(id, name, description, goal)
	await createProjectPromise
	totalProjects = totalProjects+1
	getProjects();
	$("#formProject").hide()
}

async function getProject(){
	getProjectPromise = Contract.Projects(0)
	await createProjectPromise
	console.log(createProjectPromise);
}

function getRawTransaction(tx) {
	function addKey(accum, key) {
	  if (tx[key]) { accum[key] = tx[key]; }
	  return accum;
	}
  
	// Extract the relevant parts of the transaction and signature
	const txFields = "accessList chainId data gasPrice gasLimit maxFeePerGas maxPriorityFeePerGas nonce to type value".split(" ");
	const sigFields = "v r s".split(" ");
  
	// Seriailze the signed transaction
	const raw = utils.serializeTransaction(txFields.reduce(addKey, { }), sigFields.reduce(addKey, { }));
  
	// Double check things went well
	if (utils.keccak256(raw) !== tx.hash) { throw new Error("serializing failed!"); }
  
	return raw;
  }

async function getMood() {
	getMoodPromise = MoodContract.getMood()
	var Mood = await getMoodPromise
	console.log(Mood)
}

async function getProjects() {
	$('#tableProjects').html('<tr class="warning no-result"><td colspan="12"><i class="fa fa-warning"></i>&nbsp; No Result !!!</td></tr>');
	projects = []
	for (var i = 0; i < totalProjects; i++) {
		const project = await Contract.projects(i);
		projects.push(project);
		//console.log('project',project)
		$('#tableProjects').append('<tr><td id="idProject-'+i+'" style="color: #8ac53f; cursor:pointer;" onclick="getContributor(&apos;'+project.id+'&apos;,1)">'+project.id+'</td><td>'+project.name+'</td><td>'+project.description+'</td><td>'+project.state+'</td><td>111111</td><td><button class="btn btn-disable" style="margin-left: 5px" type="buttom" id="stake-'+i+'" onclick="stake('+i+')" >Stake</button</td></tr>')
	}
	console.log('projects**',projects)
}

async function getContributor(id,total) {
	$('#tableProjects').html('<tr class="warning no-result"><td colspan="12"><i class="fa fa-warning"></i>&nbsp; No Result !!!</td></tr>');
	contributions = []
	for (var i = 0; i < total; i++) {
		const contribution = await Contract.contribution(id, i);
		projects.push(contribution);
		console.log('contribution',contribution)
		//$('#tableProjects').append('<tr><td>'+project.id+'</td><td>'+project.name+'</td><td>'+project.description+'</td><td>'+project.state+'</td><td>111111</td></tr>')
	}
	if(contributions.length <= 0){
		$('#tableProjects').html('<tr><td colspan="12"><i class="fa fa-warning"></i>&nbsp; No Result !!!</td></tr>');
	}
	console.log('contributions**',contributions)
}

async function stake(i) {
	console.log(i)
	
	const tx = await signer.sendTransaction({
		to: projects[i][3],
		value: 1000,
	});
	console.log(tx, 'tx')
	alert("Succesful transaction")
	// Acccounts now exposed
    /*const params = [{
        from: signer,
        to: projects[i][3],
        value: ethers.utils.parseUnits("0.05", 'ether').toHexString()
    }];

    const transactionHash = await Contract.send('eth_sendTransaction', params)
    console.log('transactionHash is ' + transactionHash);*/
	//const fundProject = await Contract.fundProject(i);
}
