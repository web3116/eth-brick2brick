;(function ($) {
	"use strict" // Start of use strict

	// Smooth scrolling using jQuery easing
	$('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
		if (
			location.pathname.replace(/^\//, "") ==
				this.pathname.replace(/^\//, "") &&
			location.hostname == this.hostname
		) {
			var target = $(this.hash)
			target = target.length
				? target
				: $("[name=" + this.hash.slice(1) + "]")
			if (target.length) {
				$("html, body").animate(
					{
						scrollTop: target.offset().top - 57,
					},
					1000,
					"easeInOutExpo"
				)
				return false
			}
		}
	})

	// Closes responsive menu when a scroll trigger link is clicked
	$(".js-scroll-trigger").click(function () {
		$(".navbar-collapse").collapse("hide")
	})

	// Activate scrollspy to add active class to navbar items on scroll
	$("body").scrollspy({
		target: "#mainNav",
		offset: 57,
	})

	// Collapse Navbar
	var navbarCollapse = function () {
		if ($("#mainNav").offset().top > 100) {
			$("#mainNav").addClass("navbar-shrink")
		} else {
			$("#mainNav").removeClass("navbar-shrink")
		}
	}
	// Collapse now if page is not at top
	navbarCollapse()
	// Collapse the navbar when page is scrolled
	$(window).scroll(navbarCollapse)
	let activeFormProject = false
	$("#btnFormProject").click(function () {
		if (!activeFormProject) {
			$("#formProject").show()
			activeFormProject = true
		} else {
			$("#formProject").hide()
			activeFormProject = false
		}
	})
	// create proyect
	$("#btnSave").click(function () {
		const id = "id" + Math.floor(Math.random() * 100).toString()
		const name = $("#name").val()
		const description = $("#description").val()
		const goal = $("#goal").val()
		createProject(id, name, description, goal)
	})

  $("#getProjects").click(function () {
		getProjects()
	})
  

	// // connect wallet
	// $("#connect").click(function () {
	// 	// connectWallet()
	// })

// connect wallet
	$(function () {
		if (window.location.hash && window.location.hash == "#signin") {
			connectWallet()
		}
	})

	// Magnific popup calls
	$(".popup-gallery").magnificPopup({
		delegate: "a",
		type: "image",
		tLoading: "Loading image #%curr%...",
		mainClass: "mfp-img-mobile",
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0, 1],
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
		},
	})
})(jQuery) // End of use strict

